## **Aufgabenstellung**
Das Ziel ist es, einen Prototyp für die Integration eines **Multi-Browser-Testing Tools([Sauce Labs](https://bitbucket.org/Trapu20/multi-browsing-testing-tool/wiki/Was%20ist%20Sauce%20Labs%3F))** in eine **[CI(Jenkins)](https://bitbucket.org/Trapu20/multi-browsing-testing-tool/wiki/Was%20ist%20Jenkins%20CI%3F)** Umgebung zu erstellen. 

Mit Hilfe dieses Prototyps soll gezeigt werden, wie ein Multi-Browser-Testing Tools in eine Jenkins CI Umgebung integriert werden kann. Als Ergebnis der Integration soll eine automatische Ausführung von GUI Tests auf unterschiedlichen Browsern möglich sein. Der Report soll im Jenkins integriert angezeigt werden.

In einer erweiterten Ausbaustufe, kann man durch das Anklicken der Tests detailliertere Ergebnisse sehen. (evtl. Screenshots bei Fehlern) 

---

## ** Projekt-Info**
Das Projekt **ToDoList** ist ein Demo-Projekt, um unsere Aufgabe bzw. unser Ziel zu veranschaulichen.

Es selbst stellt ein API dar, mittels der man auf das Klicken der UI **GET**, **POST**, **DELETE** und **UPDATE** Anfragen abschicken kann. Das Projekt ist aufgeteilt in die 3-Schichten-Architektur. Die Anfragen werden von einem Manager verwaltet und über die eigene Datanbank-Schicht an die Datenbank weitergeleitet. Die aktuell in der Datenbank stehenden Daten werden nun tabellarisch angezeigt. 

Geschrieben wurde dieses Projekt in **[node.js](https://nodejs.org/)** und für die Oberfläche ist **[Angular.js](https://angular.io/)** zuständig.

---

## **Versionsverwaltung**
Für die Versionsverwaltung benutzen wir ausschließlich **[Git](https://github.com/)** in Kombination mit **[Bitbucket](https://bitbucket.org/)**.

### **Einleitung (Terminal)**

**Erstmalig das Verzeichnis kopieren**

Sollte ich das Projekt nicht in meinem Verzeichnis haben, sondern es liegt nur auf dem Git-Verzeichnis, muss ich in einem leeren Verzeichnis Folgendes ausführen:

```
        git clone <<GIT-URL>>
```

**Code Pushen**

Sollte ich das Projekt auf meinem Verzeichnis haben und möchte es in das Git-Verzeichnis schieben, muss ich in meinem Verzeichnis Folgendes ausführen:

```
        cd existing-project
        git init
        git add --all
        git commit -m "Initial Commit"
        git remote add origin <<GIT-URL>>
        git push -u origin master
```

**Code Pullen**

Sollte ich das Projekt auf meinem Verzeichnis haben, jedoch ist es nicht die aktuellste Version, die auf dem Git-Verzeichnis liegt, muss ich Folgendes ausführen:

```
        git pull <<GIT-URL>> master
```

---

## **CI Umgebung**
Die CI Umgebung, die wir für unser Projekt verwenden, ist **[Jenkins CI](https://bitbucket.org/Trapu20/multi-browsing-testing-tool/wiki/Was%20ist%20Jenkins%20CI%3F)**. 

Um ein Projekt in Jenkins integrieren zu können, brauch das jeweilige Projekt ein sogenanntes **Jenkinsfile**. In diesem Jenkinsfile werden die einzelnen Stufen (=stages) in einer **[Pipeline](https://jenkins.io/doc/book/pipeline/)** beschrieben.

**Beispiel:**
```
pipeline {
    agent { docker { image 'node:6.3' } }
    stages {
        stage('build') {
            steps {
                echo 'Define here, what the stage called 'build' should do here.'
            }
        }
    }
}
```

In unserem Projekt liegt dieses Jenkinsfile ganz vorne. 

![Repo-Structure](https://bitbucket.org/Trapu20/multi-browsing-testing-tool/downloads/Capture.png")


---

## **UI-Testing**

---

## **How-To: Install**
Zuerst, muss man das Projekt aus dem Git-Verzeichnis ziehen(siehe **Erstmalig das Verzeichnis kopieren**). 
Nachdem man erfolgreich das Projekt auf seinem Rechner hat, muss der Chrome-Treiber installiert werden.

1. Die aktuellste Treiber-Version herunterladen: http://chromedriver.chromium.org/
2. Anschließend in den Umgebungsvariablen den Pfad des entpackten Treibers in die **PATH** Tabelle hinzufügen.