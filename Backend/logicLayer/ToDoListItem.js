﻿
module.exports = (function () {
    var _Id = 0;
    var _State = {
        ToDo: 1,
        Cancel: 2,
        Finish: 3
    };
    function _CreateToDoListItem(newTitle, newDesc) {
      
        if (typeof newDesc == 'string' && typeof newTitle == 'string') {
            _Id++;
            var _Desc = newDesc;
            var _Title = newTitle;
        }

        return {
            Id: _Id,
            Title: _Title,
            Desc: _Desc,
            State: _State
        }
    }

    return {
        CreateToDoListItem : _CreateToDoListItem,
    }
})();