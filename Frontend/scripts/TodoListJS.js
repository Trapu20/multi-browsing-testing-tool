var app = angular.module('myApp', ["ngRoute"]);
app.controller('myCtrl', function($scope,$http) {

  

    $scope.getTDLs = function(){           
            $http.get("/api/todolists").then(function(response) {
               $scope.toDoLists = response.data;
               $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Success!</strong> Successfully loaded all lists');
                $scope.AlertMessage = "success";   
                $scope.LogMessage = "Successfully TodoLists loaded" 
           }).catch(function(err){
                $scope.AlertMessage = "danger";
                 $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
                 $scope.LogMessage = "Error occured by loading all lists";
           });                                                                                   
    }    

    $scope.editTodoList = function(name, owner,nr){
        $scope.CurrentToDolistToEdit.ListName = name;
        $scope.CurrentToDolistToEdit.Owner = owner;
        console.log(nr);
    }
    $scope.getItemsOfList = function(id){

        $http.get("/api/todolists/"+id+"/items").then(function(response) {           
            $scope.toDoListItems = response.data; 
            $scope.CurrentToDoListIdThatShowItems = id;       
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Success!</strong> Successfully loaded all lists');
            $scope.AlertMessage = "success";   
            $scope.LogMessage = "Successfully TodoLists loaded" 
        }).catch(function(err){
             $scope.AlertMessage = "danger";
             $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
             $scope.LogMessage = "Error occured by loading all lists";
        });        
    }
    $scope.createNewToDoList = function(Name,Owner){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistToCreate = { "Title" : Name,
                                "Owner" : Owner};
        $http.post("/api/todolists",JSON.stringify(todolistToCreate),config).then(function(response){
                console.log(response.data);
                $scope.getTDLs();
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }

    $scope.createNewToDoListItem = function(title,Desc){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistItemToCreate = { 
                	            "ListId": { "Id" : $scope.CurrentToDoListIdThatShowItems },
                                "Title" : title,
                                "Desc" : Desc
                            };
                                
        $http.post("/api/todolists/items",JSON.stringify(todolistItemToCreate),config).then(function(response){
                console.log(response.data); 
                $scope.getItemsOfList(todolistItemToCreate.ListId.Id);             
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }
    $scope.deleteToDolist = function(Name,Owner,ListId){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistToDelete = { "Title" : Name,
                                "Owner" : Owner,
                                "Id": ListId
                            };
               

        $http.delete("/api/todolists/"+ListId,JSON.stringify(todolistToDelete)).then(function(response){
                console.log(response.data);
                $scope.getTDLs();                
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }
    $scope.deleteToDolistItem = function(Name,Owner,ListId){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistToDelete = { "Title" : Name,
                                "Owner" : Owner,
                                "Id": ListId
                            };               
        $http.delete("/api/todolists/Items/"+ListId,JSON.stringify(todolistToDelete)).then(function(response){
                console.log(response.data);
                $scope.deleteToDolist(Name,Owner,ListId);                            
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }
      //#region 
      $scope.CurrentToDolistToEdit = { "ListName" : "","Owner" : "" } ;
      $scope.VisibilityLogStatus = false;
      $scope.LogMessage = " test text";
      $scope.CurrentToDoListThatShowItems;
      $scope.toDoLists = $scope.getTDLs();
      //#endregion
});
app.config(function($routeProvider) {
    $routeProvider
    .when("/items", {
        templateUrl : "ToDolistsShowItems.html"
    }).when("/",{
        templateUrl:"TodoLists.html"
    });   
});