var express = require('express');
var tdlRouter = express.Router();
var toDoListManager = require('../logiclayer/TDLManager');


tdlRouter.get('/', function(req, res) {
    //res.json({ message: 'hooray! welcome to our api!' }); 
    toDoListManager.GetLists(function(lists){
     //todo: add hyperlinks to objects
      res.json(lists);
    },function(err){
      res.writeHead(500,"Internal server Error");
      res.end();
      //   res.write(JSON.stringify(err));
     // res.end();
    }); 
});
tdlRouter.get('/:id', function(req, res) { 
  var tdlid = req.params.id;    
    toDoListManager.GetList(Number(tdlid),function(selectedlists){
      res.status(200);
      res.json(selectedlists);
      res.end();
    },function(err){
      //ToDo: check err object which type of errroe has occured
      // for example 500? 404? something else
      if(err.message.toLowerCase() === "not found"){
        res.status(404).send("List not found with given ID : "+tdlid);
      }else {
        res.status(500).send("Something gone wrong");
      }   
      res.end();
    });
   
});

tdlRouter.post('/',function(req, res) {
  var newTDL = req.body;
  newTDL = toDoListManager.CreateList(newTDL,function(TDLToReturn){
    //todo: add hyperlinks to objects
     console.log("Inserted TDL: "+TDLToReturn);
     res.status(201);
     res.json(TDLToReturn); 
     res.end();
   },function(err){
     res.writeHead(500,"Internal server Error. Could not insert. Error: "+err.message);
     res.end();
   });   
});

tdlRouter.post('/items',function(req, res) {
  var newTDL = req.body;
  newTDL = toDoListManager.CreateItem(newTDL.ListId,newTDL.Title,newTDL.Desc,function(TDLToReturn){
    //todo: add hyperlinks to objects
     console.log("Inserted TDL: "+TDLToReturn);
     res.status(201);
     res.json(TDLToReturn); 
     res.end();
   },function(err){
     res.writeHead(500,"Internal server Error. Could not insert. Error: "+err.message);
     res.end();
   });   
});

tdlRouter.delete('/:id',function(req, res) {
  var newTDL = {};
  newTDL.Id = req.params.id;  

  toDoListManager.DeleteToList(newTDL,function(TDLToReturn){
    //todo: add hyperlinks to objects
     console.log("Deleted TDL: "+TDLToReturn);
     res.status(200);
     res.json(TDLToReturn); 
     res.end();
   },function(err){
     res.writeHead(500,"Internal server Error. Could not Delete. Error: "+err.message);
     res.end();
   }); 
  
  
});

tdlRouter.delete('/Items/:id',function(req, res) {
  var newTDL = {};
  newTDL.Id = req.params.id;  

  toDoListManager.DeleteToListItem(newTDL,function(TDLToReturn){
    //todo: add hyperlinks to objects
     console.log("Deleted TDLItem: "+TDLToReturn);
     res.status(200);
     res.json(TDLToReturn); 
     res.end();
   },function(err){
     res.writeHead(500,"Internal server Error. Could not Delete. Error: "+err.message);
     res.end();
   }); 
  
  
});
tdlRouter.get('/:id/items', function(req, res) { 
  var newTDL = {};
  newTDL.Id = req.params.id;    
    toDoListManager.GetItems(newTDL,function(selectedItems){
      res.status(200);
      res.json(selectedItems);
      res.end();
    },function(err){
      //ToDo: check err object which type of errroe has occured
      // for example 500? 404? something else
      if(err.message.toLowerCase() === "not found"){
        res.status(404).send("Items not found of List with given ID : "+newTDL.Id);
      }else {
        res.status(500).send("Something gone wrong");
      }   
      res.end();
    });
   
});


module.exports = tdlRouter;