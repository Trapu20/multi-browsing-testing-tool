var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {

    $scope.CurrentToDolistToEdit = { "ListName" : "","Owner" : "" } ;
   
    $scope.toDoLists= "";
  

    $scope.getTDLs = function(){           
            $http.get("/api/todolists").then(function(response) {
               $scope.toDoLists = response.data;
               $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Success!</strong> Successfully loaded all lists');
            $scope.AlertMessage = "success";   
            $scope.LogMessage = "Successfully TodoLists loaded" 
           }).catch(function(err){
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
           });                                                                                   
    }    

    $scope.editTodoList = function(name, owner,nr){
        $scope.CurrentToDolistToEdit.ListName = name;
        $scope.CurrentToDolistToEdit.Owner = owner;
        console.log(nr);
    }
    $scope.createNewToDoList = function(Name,Owner){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistToCreate = { "Title" : Name,
                                "Owner" : Owner};
        $http.post("/api/todolists",JSON.stringify(todolistToCreate),config).then(function(response){
                console.log(response.data);
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }

    $scope.deleteToDolist = function(Name,Owner,ListId){
        var config = {
            headers : {
                'Content-Type': 'application/json'
            }
        }
        var todolistToDelete = { "Title" : Name,
                                "Owner" : Owner,
                                "Id": ListId
                            };
                            
        $http.delete("/api/todolists/"+ListId,JSON.stringify(todolistToDelete)).then(function(response){
                console.log(response.data);
                alert(response.data);
        }).catch(function (err){
            console.error(err.message);
            $scope.AlertMessage = "danger";
            $('#LogMessagesDiv').html('<button type="button" class="close" data-hide="alert">&times;</button> <strong>Error!</strong>Error occured by loading all lists');
            $scope.LogMessage = "Error occured by loading all lists";
        });
    }
});