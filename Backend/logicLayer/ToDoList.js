﻿module.exports = (function () {  

    function _CreateToDoList(newId,newTitle, newOwner) {
        var title;
        var owner;
        var id;
        var items = [];
        if (typeof newOwner == 'string' && typeof newTitle == 'string' && typeof newId == 'number') {
             owner = newOwner;
             title = newTitle;
                id = newId;
        }

        return {
            Title: title,
            Owner: owner,
            Items: items,
            Id: id
        }
    }


    return {
        CreateToDoList: _CreateToDoList,       
    }
})();