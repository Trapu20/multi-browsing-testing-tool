var webdriver = require('selenium-webdriver'),
    username = "RaphaelN",
    accessKey = "47522d11-af92-4bc6-bcf3-5929c3b9cff6",
    driver;
 
var SauceURL ="http://" + username + ":" + accessKey +
"@ondemand.saucelabs.com:80/wd/hub";

var browser ="chrome-mac-saucelabs";
runWithBrowserSettings(browser);
var sessionID;
driver.getSession().then(function(session){
  sessionID = session.getId();
  console.log('SauceOnDemandSessionID='+sessionID + ' job-name=MacTest');
  process.stdout.write('SauceOnDemandSessionID='+sessionID + ' job-name=MacTest');

  driver.get('http://www.google.com/ncr');
  driver.findElement(webdriver.By.name('q')).sendKeys('I BIMS 1 NODE.JS APP', webdriver.Key.ENTER);
  
  driver.quit();
});





function runWithBrowserSettings(browser){
  switch(browser){
    case "chrome-win-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'chrome',
        'platform': 'OS X 10.11',
        'version': '68.0',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
  
    case "chrome-mac-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'chrome',
        'platform': 'Windows 7',
        'version': '68.0',
        'name': 'MacTest',
        'build': 'MacTest',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
    case "firefox-win-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'firefox',
        'platform': 'Windows 7',
        'version': '50.0',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
    case "firefox-mac-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'firefox',
        'platform': 'OS X 10.11',
        'version': '50.0',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
    case "safari-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'safari',
        'platform': 'OS X 10.11',
        'version': '10.0',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
    case "ie-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'internet explorer',
        'platform': 'Windows 10',
        'version': '10.0',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
    case "edge-saucelabs":
      driver = new webdriver.Builder().
      withCapabilities({
        'browserName': 'MicrosoftEdge',
        'platform': 'Windows 10',
        'version': '16.16299',
        'username': username,
        'accessKey': accessKey
      }).
      usingServer(SauceURL).
      build();
    break;
  }
}