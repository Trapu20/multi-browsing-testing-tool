// todolistMain.js

var port = process.env.PORT || 9000;        // set our port
// call the packages we need
var express    = require('express');        // call express
var bodyParser = require('body-parser');


// custom packages module we need 
var defaultrouter = require('./serviceLayer/defaultRouter');
var tdlRouter = require('./serviceLayer/tdlRouter');

// create and configure the app
var app = express();  
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', defaultrouter);
app.use(express.static('Frontend'));
defaultrouter.use('/todolists',tdlRouter);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
