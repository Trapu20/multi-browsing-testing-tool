﻿var todolistitem = require('./ToDoListItem');
var todoList = require('./ToDoList');
//var DAL = require('../dataLayer/tdlDataAccessLayerSQL');

module.exports = (function () {
    var _managedLists = [];
    var idLists = 0;
    function _ArgumentException(message) {
        this.message = message;
        this.name = "Argument Exception";
    }

    function _CreateItem(list, title, desc,cbfSuccess,cbfError) {
        if (typeof list == 'object' && typeof title == 'string' && typeof desc == 'string') {
            // var checkListExists = _managedLists.some(function (element) {
            //     return element == list;
            // });
            var checkListExists =true;;          
            //if (checkListExists == true) {
                var toDoListItem = todolistitem.CreateToDoListItem(title, desc);
                toDoListItem.State.ToDo;               
                //DAL.createListItem(list.Id,toDoListItem,cbfSuccess,cbfError);
            // }
            // else
            // {
            //     throw new _ArgumentException("The list doesn' t exist. You cannot create an item without a valid list!");
            // }
        } 
        else 
        {
            throw new _ArgumentException("Your parameter have the wrong type!");
        }
    }
    
    function _CreateList(newTDL,cbfSuccess,cbfError) {
        if (typeof newTDL.Title == 'string' && typeof newTDL.Owner == 'string') {
            if (newTDL.Title != "" && newTDL.Owner != "") {                               
                    var list = todoList.CreateToDoList(idLists,newTDL.Title, newTDL.Owner);
                    _managedLists.push(list);               
                   // DAL.insertList(list,cbfSuccess,cbfError);               
            } else {
                throw new _ArgumentException('Not valid Argument');
            }
        } else {
            throw new _ArgumentException('Not valid Argument');
        }
        return list;
    }
    
    function _SetStateOfItem(item, state) {
        if (typeof item == 'object' && typeof state == 'string') {
            var index = _managedLists.findIndex(function (elem) {
                return elem == item;
            });
            
            switch (state) {
                case 'Finish':
                    _managedLists[index].State.Finish;
                    break;
                case 'ToDo':
                    elem.State.ToDo;
                    break;
                case 'Cancel':
                    elem.State.Cancel;
                    break;
            }
        } else {
            throw new _ArgumentException("Please check the type of your arguments!");
        }
    }

    function _getlist(id,cbfSuccess,cbfError){
        
         //DAL.getListById(id,cbfSuccess,cbfError);
    }
    function _Reset() {
        _managedLists = [];
    }

    function _FinishItem(item) {
        _SetStateOfItem(item, 'Finish');
    }

    function _CancelCancelItem(item) {
        //ToDo: ReCancel the Cancel Item
        //Verantwortlich: Gerges
        _SetStateOfItem(item, 'ToDo');
    }

    function _CancelItem(list,itemid) {
        //ToDo: Cancel Item by item ID 
        //Verantwortlich: Gerges
        if (typeof list == 'object' && typeof itemid == 'number') {
            var index = list.findIndex(function (elem) {
                return elem.Id == itemid;
            })
        } else {
            throw new _ArgumentException("Your arguments have the wrong type!");
        }

        list[index].State.Cancel;
    }
    function _CancelItem(item) {
        //ToDo: Cancel Item by item
        //Verantwortlich: Gerges
        _SetStateOfItem(item, 'Cancel');
    }

    function _GetItems(list,cbfSuccess,cbfError) {
        //ToDo: Retrurn all items of a list
        //Verantwortlich: Trapuzzano
        //return DAL.getListitemsByListId(list,cbfSuccess,cbfError);
    }

    function _GetLists(cbfSuccess,cbfError) {
         //Verantwortlich: Trapuzzano
        //return _managedLists;
        //ToDo: do business Logic Checks Here
        //return DAL.getLists(cbfSuccess,cbfError);
    }
    function _insertState(cbfSuccess) {
        //Verantwortlich: Trapuzzano
       //return _managedLists;
       //ToDo: do business Logic Checks Here
       //return DAL.insertState(cbfSuccess);
   }

    function _deleteToDoList(newTDL,cbfSuccess,cbfError){                            
       
           // DAL.deleteList(newTDL,cbfSuccess,cbfError);        
    }
    function _deleteToDoListItem(newTDL,cbfSuccess,cbfError){
        //DAL.deleteListItem(newTDL,cbfSuccess,cbfError);
    }

    return {
        CreateItem: _CreateItem,
        FinishItem: _FinishItem,
        CreateList: _CreateList,
        CancelCancelItem: _CancelCancelItem,
        CancelItem: _CancelItem,
        GetItems: _GetItems,
        GetLists: _GetLists,
        GetList: _getlist,
        InsertState: _insertState,
        Reset: _Reset,
        DeleteToList: _deleteToDoList,
        DeleteToListItem: _deleteToDoListItem
    };
})();