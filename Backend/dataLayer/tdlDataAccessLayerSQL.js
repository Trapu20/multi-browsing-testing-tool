//import { pool } from './C:/Users/gianl/AppData/Local/Microsoft/TypeScript/2.6/node_modules/@types/mssql';

var todoListDAL = {};
const sql = require('mssql');

const config = {
    user: 'dbuser',
    password: 'dummy1234',
    server: 'localhost',
    database: 'ToDoList_13',
    options: {
        instanceName: 'SQLEXPRESS',
    }
};

todoListDAL.getLists = function (cbfSuccess, cbfError) {
    // toDo: replace mockupdata
    var query = 'select * from [ToDoList];';
    executeQuery(query, function (records) {
        cbfSuccess(convertSqlToObjects(records));
    }, cbfError);
}
todoListDAL.getListById = function (id, cbfSuccess, cbfError) {   
    var query = 'select * from [ToDoList] where ToDoListId =' + id + ';';
    executeQuery(query, function (records) {
        var selectedlist = convertSqlToObjects(records)[0];     
        if (selectedlist == undefined) {
            cbfError(new Error("not found"));
        } else {
            cbfSuccess(selectedlist);
        }
    }, cbfError);

}
todoListDAL.insertList = function (newTDL, cbfSuccess, cbfError) {
    try {
       // executeInsertToDoListQuery("INSERT INTO [ToDoList]  ([Owner],[Title]) OUTPUT INSERTED.ToDoListId VALUES ('" + newTDL.Owner + "','" + newTDL.Title + "')",newTDL, cbfSuccess, cbfError);
       var query = "INSERT INTO [ToDoList] ([Owner],[Title]) OUTPUT INSERTED.ToDoListId VALUES ('" + newTDL.Owner + "','" + newTDL.Title + "');";
        executeQuery(query,function(result){
            newTDL.Id = result[0].ToDoListId;
            cbfSuccess(newTDL);        
        },cbfError);
    } catch (ex) {
        throw ex;
    }
}
todoListDAL.insertState = function (cbfSuccess, cbfError) {
    // toDo: replace mockupdata
    var lists = [];
    lists.push({ StateId: 1, Statetext: 'ToDo' });
    sql.connect(config, err => {
        // ... error checks
        if (err) {
            console.log(err);
        } else {

            console.log("connected");
            new sql.Request().query("INSERT INTO [State]([StateId],[Statetext]) VALUES(" + lists[0].StateId + ",'" + lists[0].Statetext + "')"
                , (err, result) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.dir(result);
                        cbfSuccess(result);
                    }
                });
        }
    });


}
todoListDAL.deleteList = function(newTDL,cbfSuccess,cbfError){
    try{
        executeQuery("DELETE FROM [ToDoList] WHERE ToDoListId = "+newTDL.Id+";",cbfSuccess,cbfError);
    }catch(ex){
        throw ex;
    }
}
todoListDAL.deleteListItem = function(newTDL,cbfSuccess,cbfError){
    try{
        executeQuery("DELETE FROM [Item] WHERE ToDoListId = "+newTDL.Id+";",cbfSuccess,cbfError);
    }catch(ex){
        throw ex;
    }
}

todoListDAL.getListitemsByListId = function(TDL,cbfSuccess,cbfError){
    try{
        executeQuery("Select * from [Item] where TodoListid = "+TDL.Id+";",function (records){
            var selectedItems = convertSqlToObjectsToDoListItems(records);     
            if (selectedItems == undefined) {
                cbfError(new Error("not found"));
            } else {
                cbfSuccess(selectedItems);
            }
            
        }, cbfError)
    }catch(ex){
        throw ex;
    }
}

todoListDAL.updateList = function (updateList,cbfSuccess,cbfError){
    try{
        executeQuery("UPDATE [ToDoList] SET [Owner] = '"+updateList.Owner+"',[Title] = "+updateList.Title +"' WHERE  TodoListId = "+updateList.Id+";",function(result){

        }, cbfError)
    }catch(ex){
        throw ex;
    }
}
todoListDAL.createListItem = function(ListId,ListItem,cbfSuccess,cbfError){
    try {            
        var query = "INSERT INTO [Item] ([Title],[Description],[StateID],[ToDoListId]) OUTPUT INSERTED.ItemId VALUES ('" + ListItem.Title + "','" + ListItem.Desc + "',"+ListItem.State.ToDo+","+ListId+");";
         executeQuery(query,function(result){
            ListItem.Id = result[0].ItemId;
             cbfSuccess(ListItem);        
         },cbfError);
     } catch (ex) {
         throw ex;
     }
}

/*function executeQuery2(query,cbfSuccess,cbfError){
    new sql.ConnectionPool(config).connect()
    .then(pool => {
        pool.request().query(query,(err,sqlResul) => {
            if(err){
                cbfError()
                return;
            }
            cbfSuccess
        })
    })
    .catch(err => {
        cbfError();
    })
}*/
// function executeInsertToDoListQuery(query,tdlList, cbfSuccess, cbfError) {
//     var lists = [];
//     sql.connect(config, err => {
//         // ... error checks
//         if (err) {
//             console.log(err);
//             sql.close();
//         } else {
//             console.log("connected");
//             new sql.Request().query(query, (err, result) => {
//                 sql.close();
//                 if (err) {
//                     console.log(err);
//                     if (typeof (cbfError) === 'function') {
//                         cbfError(err);
//                     }
//                 } else {
//                     console.dir(result);                    
//                     tdlList.Id = result.recordset[0].ToDoListId;
//                     cbfSuccess(tdlList);
//                 }
//             });
//         }
//     });
// }

function  executeQuery(query, cbfSuccess, cbfError) {
    var lists = [];
    sql.connect(config, err => {
    // ... error checks
        if (err) {
            sql.close();
            console.log(err);
        } else {
            console.log("connected");
            new sql.Request().query(query, (err, result) => {
                sql.close();
                if (err) {
                    console.log(err);
                    if (typeof (cbfError) === 'function') {
                        cbfError(err);
                    }
                } else {
                    console.dir(result);
                    cbfSuccess(result.recordset);
                }
            });
        }
    });
}


function convertSqlToObjects(records) {
    var returnResult = [];
    for (var idx = 0; idx < records.length; idx++) {
        returnResult.push({
            "ListId": records[idx].ToDoListId,
            "Owner": records[idx].Owner.trim(),
            "Title": records[idx].Title.trim(),            
        });

    }
    return returnResult;
}
function convertSqlToObjectsToDoListItems(records) {
    var returnResult = [];
    for (var idx = 0; idx < records.length; idx++) {
        returnResult.push({
            "ListItemsId": records[idx].ItemId,
            "Desc": records[idx].Description.trim(),
            "Title": records[idx].Title.trim(),
            "State": records[idx].StateID,
            "ToDoListId": records[idx].ToDoListId            
        });

    }
    return returnResult;
}
module.exports = todoListDAL;