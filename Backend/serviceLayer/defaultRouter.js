var express = require('express');
var defaultrouter = express.Router();

defaultrouter.get('/', function (req, res) {
    res.json({
        message: 'hooray! welcome to our ToDoListApi!',
        apilink1: 'http://localhost:8080/api/todolists'
    });
});

module.exports = defaultrouter;